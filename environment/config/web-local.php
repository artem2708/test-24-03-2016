<?php
return [
    'bootstrap' => ['debug'],

    'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
    ],

    'modules' => [
        'debug' => 'yii\debug\Module',
    ],
];
