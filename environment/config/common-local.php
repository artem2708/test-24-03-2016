<?php
return [
    'bootstrap' => ['gii'],

    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=rgk',
            'charset' => 'utf8mb4',
            'username' => 'root',
            'password' => 'root',
        ],

        'mailer' => [
            'useFileTransport' => true,
        ],
    ],

    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
];
