<?php

namespace app\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property string $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $date
 * @property string $preview
 * @property string $author_id
 *
 * @property Author $author
 */
class Book extends ActiveRecord
{
    /**
     * @const string
     */
    private  $imagePath = '/files/previews';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['date_create'],
                    static::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя книги',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'date' => 'Дата выхода книги',
            'preview' => 'Превью',
            'file_input' => 'Превью',
            'author_id' => 'Автор',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    public static function PreviewsFilesPath(){
        return 1;//$this->imagePath;
    }


    /**
     * @return null|string
     */
    public function getPreviewPath(){
        if (!empty($this->preview)){
            return Yii::$app->getBasePath().DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.$this->preview;
        } else {
            return null;
        }
    }
}
