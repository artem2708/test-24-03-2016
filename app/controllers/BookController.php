<?php
namespace app\controllers;

use app\entities\Book;
use app\models\BookForm;
use app\models\BookSearch;
use app\models\ImageUploadForm;
use app\repositories\BookRepository;
use app\services\ImageUploadService;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class BookController extends BaseController
{
    /**
     * @var BookRepository
     */
    protected $books;
    /**
     * @var ImageUploadService
     */
    protected $uploader;


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $this->books = new BookRepository();
        $this->uploader = new ImageUploadService();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = Book::findOne($id);
        $this->ensureThat($model);

        return $this->renderPartial('_view', compact('model'));
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $redirectUrl = Yii::$app->session->get('book-create-url');
        if (is_null($redirectUrl)){
            $redirectUrl = Yii::$app->request->referrer;
            Yii::$app->session->set('book-create-url', $redirectUrl);
        }

        $model = new BookForm();

        $imageModel = new ImageUploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $imageModel->file = UploadedFile::getInstance($imageModel, 'file');

            if ($imageModel->validate()) {
                $filePath = $this->uploader->saveImage($imageModel->file);
                $model->preview = $filePath;
            }

            $this->books->create($model->attributes);

            Yii::$app->session->remove('book-create-url');

            return $this->redirect($redirectUrl);
        }

        return $this->render('create',compact('model', 'imageModel'));
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionUpdate($id)
    {
        $book = $this->books->getById($id);
        $this->ensureExists($book);

        $redirectUrl = Yii::$app->session->get('book-update-url');
        if (is_null($redirectUrl)){
            $redirectUrl = Yii::$app->request->referrer;
            Yii::$app->session->set('book-update-url', $redirectUrl);
        }

        $model = new BookForm();
        $model->setAttributes($book->attributes);

        $imageModel = new ImageUploadForm();

        if ($model->load(Yii::$app->request->post())) {
            $imageModel->file = UploadedFile::getInstance($imageModel, 'file');

            if ($imageModel->validate()) {
                $filePath = $this->uploader->saveImage($imageModel->file);
                $model->preview = $filePath;
            }

            if ($model->validate()) {
                $data = $model->attributes;

                $this->books->update($book, $data);

                $this->redirect($redirectUrl);
            }
        }

        return $this->render('update', compact('book', 'model', 'imageModel'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $book = $this->books->getById($id);
        $this->ensureThat($book);

        $this->books->delete($book);

        return $this->redirect(Yii::$app->request->referrer);
    }
}