<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
abstract class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = 'dashboard';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['auth/login','auth/logout'],
                'rules' => [
                    [
                        'controllers' => ['auth/login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     */
    public function ensureThat($condition)
    {
        if ( ! $condition) {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     */
    public function ensureThatNot($condition)
    {
        $this->ensureThat( ! $condition);
    }

    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     */
    public function ensureExists($condition)
    {
        $this->ensureThat($condition);
    }

    /**
     * @param string  $permissionName
     * @param array   $params
     * @param boolean $allowCaching
     * @throws ForbiddenHttpException
     */
    public function ensureAllowed($permissionName, $params = [], $allowCaching = true)
    {
        if ( ! Yii::$app->user->can($permissionName, $params, $allowCaching)) {
            throw new ForbiddenHttpException('You are not allowed to access this page.');
        }
    }

    /**
     * @param string  $defaultUrl
     * @param integer $statusCode
     * @return \yii\web\Response
     */
    public function redirectBack($defaultUrl = null, $statusCode = 302)
    {
        if ($returnUrl = Yii::$app->request->getQueryParam('returnUrl')) {
            return $this->redirect($returnUrl, $statusCode);
        }

        return $this->redirect($defaultUrl, $statusCode);
    }
}