<?php
namespace app\controllers;

use app\models\LoginForm;
use app\repositories\UserRepository;
use Yii;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class AuthController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @inheritdoc
     */
    public $layout = 'empty';


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->users = new UserRepository();
    }

    /**
     * @return mixed
     */
    public function actionLogin()
    {
        if ( ! Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $this->users->getByEmail($model->email);

            if ($model->validatePassword($user)) {
                $model->login($user);
                return $this->goBack();
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
