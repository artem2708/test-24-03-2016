<?php

use yii\db\Migration;

class m160324_123534_add_table_author extends Migration
{
    public function up()
    {
        $this->createTable('authors', [
            'id' => "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
            'firstname' => "VARCHAR(20) NOT NULL",
            'lastname' => "VARCHAR(20) NOT NULL",
            "PRIMARY KEY (id)",
        ]);

        $this->batchInsert('authors', ['firstname', 'lastname'], [
            ['Федор', 'Достоевский'],
            ['Лев', 'Толстой'],
            ['Михаил', 'Булгаков'],
            ['Николай', 'Гоголь'],
            ['Александр', 'Пушкин'],
            ['Антон', 'Чехов'],
            ]);
    }

    public function down()
    {
        $this->dropTable('authors');
    }
}
