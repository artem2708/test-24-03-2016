<?php
use yii\db\Migration;

class m150720_144238_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'group' => 'VARCHAR(10) NOT NULL',
            'email' => 'VARCHAR(100) NOT NULL',
            'name' => 'VARCHAR(100) NOT NULL',
            'passwordHash' => 'VARCHAR(100) NOT NULL',
            'authKey' => 'VARCHAR(32) NOT NULL',
            'status' => 'SMALLINT(6) NOT NULL',
            'PRIMARY KEY (id)',
            'UNIQUE KEY (email)'
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
