<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m160324_123648_add_table_books extends Migration
{
    public function up()
    {
        $this->createTable('books', [
            'id' => "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
            'name' => "VARCHAR(255) NOT NULL",
            'date_create' => "DATE NOT NULL",
            'date_update' => "DATE DEFAULT NULL",
            'date' => "DATE DEFAULT NULL",
            'preview' => "VARCHAR(100) DEFAULT NULL",
            'author_id' => "INT(11) UNSIGNED DEFAULT NULL",
            "PRIMARY KEY (id)",
        ]);

        $this->addForeignKey('book_author_fk', 'books', 'author_id', 'authors', 'id');

        $items = [
            [
                'name' => 'Книга 1',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '1'
            ],
            [
                'name' => 'Книга 2',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '1'
            ],
            [
                'name' => 'Книга 3',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '1'
            ],
            [
                'name' => 'Книга 4',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '1'
            ],
            [
                'name' => 'Книга 5',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '2'
            ],
            [
                'name' => 'Книга 6',
                'date_create' => '2016-03-24',
                'date' => '2016-03-24',
                'author_id' => '2'
            ],
        ];

        $this->batchInsert('books', ['name','date_create','date','author_id'],
            ArrayHelper::getColumn($items, function ($item) {
                return [$item['name'],$item['date_create'],$item['date'],$item['author_id']];
            })
        );
    }

    public function down()
    {
        echo "m160324_123648_add_table_books cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
