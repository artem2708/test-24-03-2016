<?php
return [
    'id' => 'rgk-console',
    'controllerNamespace' => 'app\console',

    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

    ],
];
