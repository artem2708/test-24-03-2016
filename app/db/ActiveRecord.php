<?php
namespace app\db;

use Yii;
use yii\base\InvalidValueException;
use yii\web\NotFoundHttpException;

/**
 * Base active record class for application.
 *
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param mixed $condition
     * @return integer
     */
    public static function findCount($condition)
    {
        return static::find()->where($condition)->count();
    }

    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     * @return static
     */
    public static function findOr404($condition)
    {
        if ($record = static::findOne($condition)) {
            return $record;
        }

        throw new NotFoundHttpException('Record was not found in database.');
    }

    /**
     * @param mixed $condition
     * @throws InvalidValueException
     * @return static
     */
    public static function findOrFail($condition)
    {
        if ($record = static::findOne($condition)) {
            return $record;
        }

        throw new InvalidValueException('Record was not found in database.');
    }

    /**
     * @param boolean $runValidation
     * @param array   $attributeNames
     * @throws InvalidValueException
     */
    public function saveOrFail($runValidation = true, $attributeNames = null)
    {
        if ( ! $this->save($runValidation, $attributeNames)) {
            Yii::$app->raven->extraContext(['user' => $this->toArray(), 'errors' => $this->getErrors()]);
            throw new InvalidValueException('Record failed to be saved into database.');
        }
    }
}