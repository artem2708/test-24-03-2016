<?php
namespace app\helpers;

use app\contracts\Uploadable;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\BaseUrl;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class Url extends BaseUrl
{
    public static function entity($entity)
    {
        if ($entity instanceof \app\entities\Article) {
            return static::to(['articles/view', 'id' => $entity->id, 'slug' => $entity->slug]);
        }

        throw new NotSupportedException('Entity of class \'' . get_class($entity) . ' is not supported.');
    }

    /**
     * Wraps url with auth token for auto login.
     *
     * @param mixed $url
     * @param string $token
     * @param mixed $scheme
     * @return string
     */
    public static function tokenize($url, $token, $scheme = false)
    {
        $url = static::to($url, $scheme);
        return static::to(['redirect/auth', 'url' => $url, 'token' => (string) $token], $scheme);
    }

    /**
     * Returns url for uploaded file.
     *
     * @param Uploadable $entity
     * @param mixed $scheme
     * @return string
     */
    public static function uploaded(Uploadable $entity, $scheme = false)
    {
        return static::to('/' . $entity->getFileName(), $scheme);
    }
}