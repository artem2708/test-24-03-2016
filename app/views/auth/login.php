<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\models\LoginForm */
/* @var $form  yii\widgets\ActiveForm */
/* @var $this  yii\web\View  */

$this->title = 'Вход';
?>

<div class="site-login">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-signin'],
        'fieldConfig' => [
            'template' => "{input}",
        ],
    ]); ?>

    <h2 class="form-signin-heading">Вход на сайт</h2>

    <div class="login-wrap">
        <?= $form->field($model, 'email', ['template' => '{input} {error}'])->textInput(['placeholder' => 'Почта']) ?>
        <?= $form->field($model, 'password', ['template' => '{input} {error}'])->passwordInput(['placeholder' => 'Пароль']) ?>
        <?= Html::submitButton('Войти', ['class' => 'btn btn-lg btn-login btn-block', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
