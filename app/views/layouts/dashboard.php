<?php
use otsec\yii2\fladmin\DashboardAsset;
use otsec\yii2\fladmin\MainMenu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this    yii\web\View */
/* @var $content string */

DashboardAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <section id="container">
        <!--header start-->
        <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
            </div>

            <a href="/" class="logo">
                <span>RGK</span>Group
            </a>

            <div class="top-nav">
                <?php if(!Yii::$app->user->isGuest):?>
                    <ul class="nav pull-right top-menu">
                        <li class="dropdown language">
                            <a href="<?= Url::to(['auth/logout']) ?>">
                                <span class="fa fa-unlock-alt"></span>
                                <span class="username">Выход</span>
                            </a>
                    </ul>
                <? else:?>
                    <ul class="nav pull-right top-menu">
                        <li class="dropdown language">
                            <a href="<?= Url::to(['auth/login']) ?>">
                                <span class="fa fa-unlock-alt"></span>
                                <span class="username">Вход</span>
                            </a>
                    </ul>
                <?endif?>
            </div>
        </header>
        <!--header end-->

        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse">
                <?= MainMenu::widget([
                    'encodeLabels' => false,
                    'items' => [
                        [
                            'label' => '<i class="fa fa-fw fa-users"></i> Книги',
                            'url' => ['book/index'],
                            'active' => (Yii::$app->controller->id === 'book'),
                        ],
                    ],

                ]); ?>
            </div>
        </aside>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => ['label' => '<i class="fa fa-home"></i>', 'url' => ['default/index']],
                    'encodeLabels' => false,
                ]) ?>
                <?= $content; ?>
            </section>
        </section>
        <!--main content end-->

        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                <?= date('Y') ?> &copy; <?php echo Yii::$app->name; ?>.
                <a href="#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
        <!--footer end-->
    </section>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
