<?php
use app\entities\Book;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */
/* @var $searchModel \app\models\BookSearch */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'id',
            'format' => 'raw',
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function (Book $book) {
                return Html::a($book->name, ['update', 'id' => $book->id], ['class' => 'btn-block']);
            },
        ],
        [
            'attribute' => 'preview',
            'format' => 'raw',
            'value' => function (Book $book) {
                return $book->preview ? Html::img(Url::toRoute('uploads/'.$book->preview),['style'=>['width'=>'150px'],'class'=>'zoom']) : '';
            },
        ],
        [
            'attribute' => 'Автор',
            'format' => 'raw',
            'value' => function (Book $book) {
                return $book->author->firstname.' '.$book->author->lastname;
            },
        ],
        [
            'attribute' => 'date',
            'format' => 'raw',
        ],
        [
            'attribute' => 'date_create',
            'format' => 'raw',

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 120],
            'template' => '{view}{update} {delete}',
            'buttons' => [
                'view' => function ($url,$model) {
                    return Yii::$app->view->render('_options',['model' => $model]);
                }
                ]
        ],
    ],
]); ?>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <h4 class="modal-title" id="myModalLabel">Изображение</h4>
            </div>
            <div class="modal-body">
                <img src="" id="preview" style="width: 100%; height: 100%;" >
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('$(".zoom").on("click", function() {
   $("#preview").attr("src", $(this).attr("src"));
   $("#modal").modal("show");

});')
?>
