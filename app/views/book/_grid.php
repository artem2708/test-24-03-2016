<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
?>

<div class="panel">
    <div class="panel-body">

        <div class="pull-left">
            {summary}
        </div>
        <div class="pull-right">
            <?= Html::a('<span class="fa fa-plus"></span> добавить книгу', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>

    <div class="panel-body">
        {items}
        {pager}
    </div>
</div>