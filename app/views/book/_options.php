<?php
use app\entities\Book;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\entities\Book */
?>

<?
Modal::begin([
'header' => '<h4>Информация о книге</h4>',
'toggleButton' => [
    'class' => 'glyphicon glyphicon-eye-open',
    'label' => ''
]
]);

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        [
            'attribute'=>'preview',
            'format' => 'raw',
            'value' => Html::img(Url::toRoute('/uploads/'.$model->preview),['width'=>150]),
        ],
        [
            'attribute'=>'author_id',
            'value' => $model->author->firstname.' '.$model->author->lastname,
        ],
        [
            'attribute' => 'date',
            'value' => Yii::$app->formatter->asDate($model->date, 'long'),
        ],
        [
            'attribute' => 'date_create',
            'value' =>  Yii::$app->formatter->asDate($model->date_create, 'long'),
        ],
        [
            'attribute' => 'date_update',
            'value' =>  $model->date_update ? Yii::$app->formatter->asDate($model->date_update, 'long') : '',
        ]
    ]
]);


Modal::end();
?>