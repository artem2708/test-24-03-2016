<?php
/* @var $this yii\web\View */
/* @var $book app\entities\Book */
/* @var $model app\models\BookForm */
/* @var $imageModel app\models\ImageUploadForm */

$this->title = $book->name ;
$this->params['breadcrumbs'][] = ['label' =>  "Книги" , 'url' => ['index']];
$this->params['breadcrumbs'][] = $book->name;
?>

<div class="book-update">
    <?= $this->render('_form', compact('model', 'imageModel')) ?>
</div>
