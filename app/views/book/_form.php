<?php
use app\entities\Author;
use dosamigos\datepicker\DatePicker;
use otsec\yii2\bootstrap\ActiveForm;
use otsec\yii2\fladmin\FlAdmin;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this  yii\web\View */
/* @var $model app\models\BookForm */
/* @var $imageModel app\models\ImageUploadForm */

$list = ArrayHelper::map(Author::find()->all(), 'id', function (Author $item) {
    return $item->firstname.' '.$item->lastname;
});
?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

    <?= FlAdmin::beginPanel('Основная информация') ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 250]) ?>
        <?= $form->field($model, 'date')->widget(
            DatePicker::className(), [
                'language' => 'ru',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-m-d'
                ]
            ]);?>
        <?= $form->field($model, 'author_id')->dropDownList($list); ?>

    <?= $form->beginField($model, 'preview') ?>
        <label class="control-label col-sm-2" for="<?= Html::getInputId($model, 'preview') ?>">
            <?= $model->getAttributeLabel('preview') ?>
        </label>

        <div class="col-sm-10 col-md-6">
            <?php if ($model->preview): ?>
                <div class="thumbnail">
                    <?= Html::img(Url::toRoute('/uploads/'.$model->preview)) ?>
                </div>
            <?php endif ?>

            <div style="margin-bottom: 20px">
                <?= Html::activeFileInput($imageModel, 'file') ?>
                <?= Html::error($model, 'preview', ['class' => 'help-block help-block-error']) ?>
            </div>
        </div>
    <?= $form->endField() ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel() ?>
        <?= $form->beginWrapper() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Вернуться назад', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
        <?= $form->endWrapper() ?>
    <?= FlAdmin::endPanel() ?>

<?php ActiveForm::end(); ?>

