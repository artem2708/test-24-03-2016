<?php
/* @var $this yii\web\View */
/* @var $book app\entities\Book */
/* @var $model app\models\BookForm */
/* @var $imageModel app\models\ImageUploadForm */

$this->title = 'Новая книга';
$this->params['breadcrumbs'][] = ['label' =>  "Книги" , 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Новая';
?>

<div class="book-create">
    <?= $this->render('_form', compact('model', 'imageModel')) ?>
</div>
