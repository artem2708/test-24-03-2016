<?php

use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\entities\Author;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */

$list = ArrayHelper::map(Author::find()->all(), 'id', function (Author $item) {
    return $item->firstname.' '.$item->lastname;
});
$this->registerJs("$('.datepicker').datepicker()",\yii\web\View::POS_READY);
?>

<div class="company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'begin_date')->widget(
        DatePicker::className(), [
            'language' => 'ru',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd/m/yyyy'
            ]
        ]);?>
    <?= $form->field($model, 'end_date')->widget(
        DatePicker::className(), [
            'language' => 'ru',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd/m/yyyy'
            ]
        ]); ?>
    <?= $form->field($model, 'author_id')->dropDownList($list,['prompt' => 'Все']) ?>

    <div class="form-group">
        <?= Html::submitButton("Поиск", ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton("Отмена", ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
