<?php
namespace app\models;

use yii\base\Model;

class BookForm extends Model
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $author_id;
    /**
     * @var string
     */
    public $preview;

    public function rules(){
        return [
            [['name', 'author_id', 'date'], 'required'],
            [['date'], 'safe'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['preview'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'date' => 'Дата создания',
            'preview' => 'Превью',
            'author_id' => 'Автор',
        ];
    }
}