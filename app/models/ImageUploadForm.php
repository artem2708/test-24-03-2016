<?php
namespace app\models;

use app\behaviors\UploadTrait;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class ImageUploadForm extends Model
{
    use UploadTrait;

    /**
     * @var UploadedFile
     */
    public $file;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'image'],
        ];
    }

    /**
     * @return string
     */
    public function save()
    {

        $destination = 'uploads/' . $this->createFileName($this->file);

        FileHelper::createDirectory(dirname($destination));

        Image::getImagine()
            ->open($this->file->tempName)
            ->thumbnail(new Box(1000, 1000))
            ->save($destination);

        return $destination;
    }
}