<?php
namespace app\models;

use app\entities\User;
use yii\base\Model;
use Yii;
use yii\web\IdentityInterface;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class LoginForm extends Model
{
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'required'],

            ['password', 'string'],
            ['password', 'trim'],
            ['password', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Почта',
            'password' => 'Пароль',
        ];
    }

    /**
     * @param IdentityInterface $identity
     */
    public function login(IdentityInterface $identity)
    {
        Yii::$app->user->login($identity, 3600 * 24 * 30);
    }

    /**
     * Validates the password.
     *
     * @param User $user
     * @return boolean
     */
    public function validatePassword(User $user = null)
    {
        if ( ! $user || ! $user->validatePassword($this->password)) {
            $this->addError('password', 'Incorrect username or password.');
            return false;
        }

        return true;
    }
}
