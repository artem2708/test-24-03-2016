<?php
namespace app\models;

use app\entities\Book;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BookSearch extends Model
{
    /**
     * @var string
     */
    public $begin_date;
    /**
     * @var string
     */
    public $end_date;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $author_id;

    public function rules(){
        return [
            [['begin_date', 'end_date'], 'string'],
            [['author_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'begin_date' => 'Дата от',
            'end_date' => 'Дата до',
            'author_id' => 'Автор',
        ];
    }

    public function search($params)
    {
        $query = Book::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 5,]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        //$query->andFilterWhere(['id' => $this->primaryKey]);
        $query->andFilterWhere(['author_id' => $this->author_id]);
        $query->andFilterWhere(['like', 'name', $this->name]);

        if (!is_null($this->begin_date) && !empty($this->begin_date)){
            $query->andFilterWhere(['>=', 'date', $this->begin_date]);
        }

        if (!is_null($this->end_date) && !empty($this->end_date)){
            $query->andFilterWhere(['<=', 'date', $this->end_date]);
        }

        return $dataProvider;
    }

}