<?php
namespace app\behaviors;

use Yii;
use yii\web\UploadedFile;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
trait UploadTrait
{
    /**
     * Generates filename for uploaded file.
     *
     * Filename will contain subdirectories to prevent prevent folding of a large number of files in one directory.
     *
     * Example (file hash -> filename):
     * 5156ced1da78ef5f92fe11fc9a919776 -> 5/1/56ced1da78ef5f92fe11fc9a919776.png
     * ec3cd44397be8452f18e8c67a200c63e -> e/c/3cd44397be8452f18e8c67a200c63e.jpg
     *
     * If unique flag is true:
     * ec3cd44397be8452f18e8c67a200c63e -> e/c/3cd44397be8452f18e8c67a200c63e-1427687138.jpg
     *
     * @param UploadedFile $file instance of uploaded file
     * @param boolean $unique whether to uniqualize generated file name, if file hash matched.
     * If true, the timestamp will be added before file extension.
     * @return string
     */
    public function createFileName(UploadedFile $file, $unique = false)
    {
        $hash = $this->createFileHash($file->tempName);

        $subPath = substr($hash, 0, 1) . '/' . substr($hash, 1, 1);
        $baseImageName = substr($hash, 2);

        if ($unique) {
            $baseImageName .= '-' . time();
        }

        return $subPath . '/' . $baseImageName . '.' . $file->extension;
    }

    /**
     * Generates hash for specified filename.
     *
     * @param string $filepath
     * @return string
     */
    public function createFileHash($filepath)
    {
        return md5_file($filepath);
    }
}