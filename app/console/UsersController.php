<?php
namespace app\console;

use app\repositories\UserRepository;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Manages users.
 *
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->users = new UserRepository();
    }

    /**
     * Creates a new user.
     */
    public function actionCreate()
    {
        $name = Console::prompt('Enter user name: ', ['required' => true]);
        $email = Console::prompt('Enter user email: ', ['required' => true]);
        $password = Console::prompt('Enter user password: ', ['required' => true]);
        $isAdmin = Console::confirm('Make this user admin?', $default = true);

        $data['group'] = ($isAdmin) ? 'admin' : 'user';
        $data['email'] = $email;
        $data['name'] = $name;

        $this->users->create($data, $password);

        Console::output('User was created successfully.');
    }
}