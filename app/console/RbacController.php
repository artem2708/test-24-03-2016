<?php
namespace app\console;

use app\rbac\UserGroupRule;
use Yii;
use yii\console\Controller;

/**
 * Command-line interface for RBAC module.
 *
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class RbacController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = 'init';


    /**
     * Generates new set of rules.
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();
        $auth->removeAll();

        $userRule = new UserGroupRule();
        $auth->add($userRule);

        $user = $auth->createRole('user');
        $user->ruleName = $userRule->name;
        $auth->add($user);

        $admin = $auth->createRole('admin');
        $admin->ruleName = $userRule->name;
        $auth->add($admin);
        $auth->addChild($admin, $user);
    }
}
