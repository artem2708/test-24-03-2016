<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class DisableCSRF extends ActionFilter
{
    /**
     * @param Action $action
     * @throws BadRequestHttpException
     * @return boolean
     */
    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;
        return true;
    }
}