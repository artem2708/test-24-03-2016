<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class UserMustBeVerified extends ActionFilter
{
    /**
     * @param Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isEmailConfirmed) {
            return true;
        }

        if (Yii::$app->user->getHasSocialProfile()) {
            return true;
        }

        Yii::$app->session->setFlash('error', Yii::t('app/alerts', 'email-not-confirmed'));
        Yii::$app->response->redirect(['profile/index']);

        return false;
    }
}