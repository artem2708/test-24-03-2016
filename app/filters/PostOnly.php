<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class PostOnly extends ActionFilter
{
    /**
     * @param Action $action
     * @throws BadRequestHttpException
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( ! Yii::$app->request->isPost) {
            throw new BadRequestHttpException();
        }

        return true;
    }
}