<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\Response;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class JsonFormatter extends ActionFilter
{
    /**
     * @param Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }
}