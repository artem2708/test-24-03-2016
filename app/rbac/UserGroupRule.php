<?php
namespace app\rbac;

use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Checks if user group matches.
 *
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class UserGroupRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'userGroup';


    /**
     * @param integer $user
     * @param Item    $item
     * @param array   $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        $group = Yii::$app->user->identity->group;

        if ($item->name === 'admin') {
            return $group == 'admin';
        }

        if ($item->name === 'user') {
            return $group == 'admin' || $group == 'user';
        }

        return false;
    }
}