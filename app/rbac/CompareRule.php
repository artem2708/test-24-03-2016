<?php
namespace app\rbac;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class CompareRule extends Rule
{
    /**
     * @var string attribute in passed array of params that contains value to compare.
     */
    public $attribute;
    /**
     * @var mixed value to compare with.
     */
    public $value;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if ( ! $this->attribute) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
    }

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return ArrayHelper::getValue($params, $this->attribute) === $this->value;
    }
}