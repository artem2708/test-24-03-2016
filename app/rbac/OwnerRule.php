<?php
namespace app\rbac;

use yii\base\InvalidValueException;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class OwnerRule extends Rule
{
    /**
     * @var string attribute in passed array of params that contains owner id.
     */
    public $attribute;
    /**
     * @var boolean
     */
    public $inverse = false;


    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if ( ! $user) {
            return false;
        }

        $owner = ArrayHelper::getValue($params, $this->attribute);

        if ( ! $owner) {
            return false;
        }

        return ($this->inverse) ? ($owner !== $user) : ($owner === $user);
    }
}