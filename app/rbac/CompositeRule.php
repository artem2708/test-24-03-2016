<?php
namespace app\rbac;

use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class CompositeRule extends Rule
{
    /**
     * @var Rule[]
     */
    public $rules = [];


    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        foreach ($this->rules as $rule) {
            if ( ! $rule->execute($user, $item, $params)) {
                return false;
            }
        }

        return true;
    }
}