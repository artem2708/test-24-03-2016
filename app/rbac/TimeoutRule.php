<?php
namespace app\rbac;

use DateInterval;
use DateTime;
use yii\base\InvalidConfigException;
use yii\base\InvalidValueException;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class TimeoutRule extends Rule
{
    /**
     * @var string attribute in passed array of params that contains time to compare.
     */
    public $attribute;
    /**
     * @var string DateInterval specification or time in relative format
     * @see http://php.net/manual/ru/dateinterval.construct.php
     * @see http://php.net/manual/ru/dateinterval.createfromdatestring.php
     */
    public $interval;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if ( ! $this->attribute) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }

        if ( ! $this->interval) {
            throw new InvalidConfigException('The "interval" property must be set.');
        }

        $this->interval = (substr($this->interval, 0, 1) === 'P')
            ? new DateInterval($this->interval)
            : DateInterval::createFromDateString($this->interval);
    }

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $time = ArrayHelper::getValue($params, $this->attribute);

        if ( ! $time) {
            return false;
        }

        $now = new DateTime();

        $compare = new \DateTime($time);
        $compare->add($this->interval);

        return ($compare > $now);
    }
}