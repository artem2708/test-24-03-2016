<?php
namespace app\repositories;

use app\entities\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @author Atem Mironov <mironov2708@gmail.com>
 */
class UserRepository extends ActiveRepository
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        if ($params) {
            $dataProvider->query->andWhere($params);
        }

        return $dataProvider;
    }

    /**
     * @param integer $id
     * @return User|null
     */
    public function getById($id)
    {
        return User::findIdentity($id);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function getByEmail($email)
    {
        return User::find()->andWhere(['email' => $email])->active()->one();
    }

    /**
     * @param array $data
     * @param string $password
     * @return User
     */
    public function create($data, $password)
    {
        $user = new User();
        $user->setAttributes($data);
        $user->setPassword($password);
        $user->authKey = Yii::$app->security->generateRandomString();
        $this->saveOrFail($user);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     */
    public function update(User $user, $data)
    {
        $user->setAttributes($data);
        $this->saveOrFail($user);
    }

    /**
     * @param User $user
     * @param      $newPassword
     */
    public function updatePassword(User $user, $newPassword)
    {
        $user->setPassword($newPassword);
        $this->saveOrFail($user);
    }

    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $this->deleteOrFail($user);
    }
}