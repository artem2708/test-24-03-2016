<?php
namespace app\repositories;

use app\entities\Book;
use Yii;

/**
 *
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class BookRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\entities\Book';
    /**
     * @inheritdoc
     */
    public $dataProvider = [
        'class' => 'yii\data\ActiveDataProvider',
        'pagination' => [
            'defaultPageSize' => 48,
            'pageSizeLimit' => [24, 98],
            'pageSizeParam' => 'limit',
        ],
        'sort' => [
            'attributes' => ['date_create'],
        ],
    ];

    /**
     * @param integer $id
     * @return mixed
     */
    public function getById($id)
    {
        return Book::findOne(['id' => $id]);
    }

    /**
     * @param string $data
     * @return object
     */
    public function create($data)
    {
        $record = \Yii::createObject(Book::className());
        $record->setAttributes($data,false);
        $record->save();

        return $record;
    }

    /**
     * @param object $record
     * @param array  $data
     */
    public function update($record, $data)
    {
        $record->setAttributes($data,false);
        return $record->save();
    }

    /**
     * @param Book $record
     */
    public function delete($record)
    {
        $record->delete();
    }
}