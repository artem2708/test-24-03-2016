<?php
namespace app\services;

use app\behaviors\UploadTrait;
use Imagine\Image\Box;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * @author Artem Mironov <mironov2708@gmail.com>
 */
class ImageUploadService
{
    use UploadTrait;


    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function saveImage(UploadedFile $file)
    {
        $filename = $this->createFileName($file);
        $filepath = Yii::getAlias('@webroot/uploads/'.$filename);

        $this->saveFile($file->tempName, $filepath);

        return $filename;
    }

    /**
     * @param string $source
     * @param string $destination
     */
    public function saveFile($source, $destination)
    {
        FileHelper::createDirectory(dirname($destination));

        \yii\imagine\Image::getImagine()
            ->open($source)
            ->thumbnail(new Box(1000, 1000))
            ->save($destination);
    }

}