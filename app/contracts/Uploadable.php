<?php
namespace app\contracts;

/**
 * Interface for a entity, that represents uploaded file.
 *
 * @author Atem Mironov <mironov2708@gmail.com>
 */
interface Uploadable {

    public function getFileName();

}